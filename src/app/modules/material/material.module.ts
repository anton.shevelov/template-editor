import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from './components/dialog/dialog.component';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  declarations: [DialogComponent],
  imports: [CommonModule],
  exports: [
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
  ],
})
export class MaterialModule {}
