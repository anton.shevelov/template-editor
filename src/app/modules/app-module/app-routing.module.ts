import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { NOT_FOUND } from './app-routes';
import { TEMPLATE } from '@module/template-generator/template-generator-routes';

const routes: Routes = [
  { path: '', redirectTo: TEMPLATE, pathMatch: 'full' },
  {
    path: TEMPLATE,
    loadChildren: () =>
      import('../template-generator/template-generator.module').then(
        (m) => m.TemplateGeneratorModule
      ),
  },

  { path: '**', redirectTo: NOT_FOUND, pathMatch: 'full' },
  { path: NOT_FOUND, component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
