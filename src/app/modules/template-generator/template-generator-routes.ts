import { Injectable } from '@angular/core';

export const TEMPLATE = 'template';
export const VIEWER = 'viewer';

@Injectable({
  providedIn: 'root',
})
export class TemplateGeneratorsRoutes {
  public VIEWER = `/${TEMPLATE}/${VIEWER}`;
}
