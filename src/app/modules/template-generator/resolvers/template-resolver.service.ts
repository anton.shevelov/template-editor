import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Template } from '@interface/template';
import { Resolve } from '@angular/router';
import { TemplateService } from '../services/template.service';

@Injectable({
  providedIn: 'root',
})
export class TemplateResolverService implements Resolve<Observable<Template>> {
  constructor(private ts: TemplateService) {}
  resolve(
    route: import('@angular/router').ActivatedRouteSnapshot,
    state: import('@angular/router').RouterStateSnapshot
  ): Observable<Template> {
    return this.ts.getTemplateByID(route.params.id);
  }
  // getTemplateByID
}
