import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Urls } from '@const/urls';
import { GenericSubject } from 'src/app/classes/generic-subject';
import { Template } from '@interface/template';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TemplateService {
  public templates: GenericSubject<Template[]> = new GenericSubject<Template[]>(
    []
  );

  constructor(private http: HttpClient, private urls: Urls) {}

  getTemplates(): Observable<Template[]> {
    return this.http.get<Template[]>(this.urls.TEMPLATES).pipe(timeout(25000));
  }
  getTemplateByID(id: number): Observable<Template> {
    return this.http
      .get<Template>(`${this.urls.TEMPLATE}/${id}`)
      .pipe(timeout(25000));
  }
  updateTemplate(template: Partial<Template>): void {
    this.http
      .post(`${this.urls.TEMPLATE}/${template.id}`, JSON.stringify(template))
      .subscribe((response) => {});
  }
}
