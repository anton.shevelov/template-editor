import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplatesComponent } from './components/templates/templates.component';
import { TemplateGeneratorRoutingModule } from './template-generator-routing.module';
import { MaterialModule } from '@module/material/material.module';
import { TemplateViewerComponent } from './components/template-viewer/template-viewer.component';
import { TemplateEditorComponent } from './components/template-editor/template-editor.component';
import { FormsModule } from '@angular/forms';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';

@NgModule({
  declarations: [
    TemplatesComponent,
    TemplateViewerComponent,
    TemplateEditorComponent,
    SafeHtmlPipe,
  ],
  imports: [
    CommonModule,
    TemplateGeneratorRoutingModule,
    MaterialModule,
    FormsModule,
  ],
  exports: [],
})
export class TemplateGeneratorModule {}
