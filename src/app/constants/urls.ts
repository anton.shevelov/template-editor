import { environment } from '@environments/environment';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class Urls {
  private SERVER = environment.server;
  public TEMPLATES = `${this.SERVER}/templates`; // GET POST request
  public TEMPLATE = `${this.SERVER}/template`; // GET
}
