export interface Template {
  id: number;
  modified: number;
  name: string;
  template: string;
}
