import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, EMPTY } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '@module/material/components/dialog/dialog.component';

@Injectable({
  providedIn: 'root',
})
export class HttpInterceptor implements HttpInterceptor {
  constructor(public dialog: MatDialog) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        'Content-Type': 'application/json',
      },
    });

    return next
      .handle(request)
      .pipe(timeout(30000))
      .pipe(
        catchError((error) => {
          this.handleError(error);
          return EMPTY;
        })
      );
  }
  handleError(error: any) {
    this.dialog.open(DialogComponent, {
      width: '250px',
      data: {
        title: 'Network request error!',
        message: `Can't get data`,
      },
    });
  }
}
