//Install express server
const express = require('express');
const path = require('path');
const fs = require('fs');
const cors = require('cors');
const app = express();
const templates = require('./templates.json');
// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist'));
app.use(cors());
app.use(express.json());
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});

app.get('/templates', function (request, response) {
  if (templates) {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.write(JSON.stringify(templates));
  } else {
    response.writeHead(404, { 'Content-Type': 'application/json' });
    response.write(JSON.stringify({ respponse: null }));
  }
  response.end();
});
app.get('/template/:id', function (request, response) {
  const id = request.params.id;
  if (templates) {
    if (id) {
      const template =
        templates.filter((template) => {
          return template.id === Number(id);
        })[0] || null;
      response.writeHead(200, { 'Content-Type': 'application/json' });
      response.write(JSON.stringify(template));
    } else {
      response.writeHead(400, { 'Content-Type': 'application/json' });
      response.write(JSON.stringify({ respponse: null }));
    }
  } else {
    response.writeHead(404, { 'Content-Type': 'application/json' });
    response.write(JSON.stringify({ respponse: null }));
  }
  response.end();
});

app.post('/template/:id', function (request, response) {
  const id = request.params.id;
  const body = request.body;
  if (templates) {
    if (id) {
      const index = templates.findIndex((item) => {
        return item.id === Number(id);
      });
      templates[index].modified = body.modified;
      templates[index].template = body.template;
      fs.writeFileSync('templates.json', JSON.stringify(templates));
      response.writeHead(200, { 'Content-Type': 'application/json' });
      response.write(JSON.stringify({ respponse: 'OK' }));
    } else {
      response.writeHead(400, { 'Content-Type': 'application/json' });
      response.write(JSON.stringify({ respponse: null }));
    }
  } else {
    response.writeHead(404, { 'Content-Type': 'application/json' });
    response.write(JSON.stringify({ respponse: null }));
  }
  response.end();
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);
